Для запуска приложения требуется Docker + Docker Compose

Запуск:
- `cd docker`  
- `docker-compose build`  
- `docker-compose up -d`  

Для просмотра логов: `docker-compose logs -f`

После запуска контейнеров приложение будет доступно по адресу `http://localhost:1366`
