#!/usr/bin/env bash

echo "Waiting for MySQL"

while ! nc -z mysql 3306; do
  sleep 0.1
done

echo "MySQL started"
