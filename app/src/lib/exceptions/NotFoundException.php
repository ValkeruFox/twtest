<?php

namespace lib\exceptions;

use Symfony\Component\HttpFoundation\Response;
use Throwable;
use Exception;
use Twig_Environment;
use Twig_Loader_Filesystem;

class NotFoundException extends Exception
{
    /** @var Twig_Environment */
    private $twig;

    /**
     * NotFoundException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Throwable|NULL $previous
     */
    public function __construct(string $message = "", int $code = 404, Throwable $previous = NULL)
    {
        parent::__construct($message, $code, $previous);

        $loader     = new Twig_Loader_Filesystem(__DIR__ . '/../../view/');
        $this->twig = new Twig_Environment($loader);
    }

    /**
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function render404(): Response
    {
        $text = $this->twig->render('default/404.twig');

        return new Response($text, 404);
    }
}
