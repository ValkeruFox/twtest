<?php

namespace lib\EntityManager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

final class EMProvider
{
    /**
     * @var EntityManager
     */
    private static $em;

    private function __construct()
    {
    }

    /**
     * @return EntityManager
     * @throws \Doctrine\ORM\ORMException
     */
    public static function getEntityManager(): EntityManager
    {
        if (self::$em instanceof EntityManager) {
            return self::$em;
        }

        $conn = [
            'driver'   => getenv('DB_DRIVER'),
            'host'     => getenv('DB_HOST'),
            'user'     => getenv('DB_USER'),
            'password' => getenv('DB_PASSWORD'),
            'dbname'   => getenv('DB_DBNAME'),
            'charset'  => getenv('DB_CHARSET'),
        ];

        $config   = Setup::createAnnotationMetadataConfiguration([__DIR__ . '/../../Entity/'], 0, NULL, NULL, 0);
        self::$em = EntityManager::create($conn, $config);

        return self::$em;
    }
}
