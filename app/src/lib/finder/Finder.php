<?php

namespace lib\finder;

class Finder
{
    private const UTF8_MARKS = [
        'UTF-8',
        'UTF8',
        'utf8',
        'utf-8'
    ];

    /**
     * @param string $url
     *
     * @return array
     * @throws \Exception
     */
    public function findImages(string $url)
    {
        $content = $this->getContent($url);
        $matches = [];
        preg_match_all('#<img\s+(?:[^>]*?\s+)?src=(["\'])(.*?)\1#', $content, $matches);

        return array_unique($matches[2]);
    }

    /**
     * @param string $url
     * @param string $pattern
     *
     * @return int
     *
     * @throws \Exception
     */
    public function findText(string $url, string $pattern = NULL): int
    {
        $content = $this->getContent($url);

        return preg_match_all("#{$pattern}#ui", $content);
    }

    /**
     * @param string $url
     *
     * @return array
     *
     * @throws \Exception
     */
    public function findLinks(string $url): array
    {
        $content = $this->getContent($url);
        $matches = [];

        preg_match_all('#<a\s+(?:[^>]*?\s+)?href=(["\'])(.*?)\1#ui', $content, $matches);

        return array_unique($matches[2]);
    }

    /**
     * @param string $url
     *
     * @return string
     * @throws \Exception
     */
    private function getContent(string $url): string
    {
        $ch = curl_init($url);
        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_USERAGENT      => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.189 Safari/537.36 Vivaldi/1.95.1077.60',
            CURLOPT_HEADER         => true
        ]);

        $response = curl_exec($ch);
        $code     = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($code !== 200) {
            $message = curl_error($ch);
            curl_close($ch);
            throw new \Exception($message);
        }

        $headersSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $content     = substr($response, $headersSize);
        $headers     = trim(substr($response, 0, $headersSize));
        curl_close($ch);

        /* Тут проверим кодировку. Зачем?
         * Некоторые господа (например, vk.com) отдают страницы в cp1251
         */
        $encoding = $this->parseEncoding($headers);
        if (!\in_array($encoding, self::UTF8_MARKS, true)) {
            $content = iconv($encoding, 'UTF-8', $content);
        }

        return $content;
    }

    /**
     * @param string $headers
     *
     * @return string
     */
    private function parseEncoding(string $headers): string
    {
        $match = [];
        preg_match("#Content-Type: .* charset=(.*)#", $headers, $match);

        return $match[1];
    }
}
