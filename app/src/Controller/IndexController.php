<?php

namespace Controller;

use Symfony\Component\HttpFoundation\Response;

class IndexController extends Controller
{
    /**
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws \InvalidArgumentException
     */
    public function index(): Response
    {
        return $this->render('default/index.twig');
    }
}
