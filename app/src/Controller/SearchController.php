<?php

namespace Controller;

use Entity\History;
use lib\finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SearchController extends Controller
{
    private $finder;

    /**
     * SearchController constructor.
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function __construct()
    {
        parent::__construct();
        $this->finder = new Finder();
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Exception
     */
    public function text(Request $request): Response
    {
        $response = new Response();

        $url    = $request->request->get('target');
        $needle = $request->request->get('pattern');
        if (empty($needle)) {
            throw new \Exception('Пустой запрос');
        }

        $count = $this->finder->findText(
            $url,
            $needle
        );

        $newresult = new History();
        $newresult->setType('text')->setUrl($url);
        $newresult->setResult(json_encode([
            'search' => $needle,
            'result' => $count
        ]));

        $this->em->beginTransaction();
        try {
            $this->em->persist($newresult);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();

            throw $e;
        }

        if ($count !== 0) {
            $response->setContent("Текст найден {$count} раз(а)");
        } else {
            $response->setContent('Текст не найден');
        }

        return $response;
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Exception
     */
    public function links(Request $request): Response
    {
        $url   = $request->request->get('target');
        $parts = [];
        preg_match('#(http(s?):\/\/)?.*\.[A-z\d]+\/?#', $url, $parts);
        $root   = trim($parts[0], '/');
        $links  = $this->finder->findLinks($url);
        $result = [];

        foreach ($links as $link) {
            if (strpos($link, '//') === 0) {
                $result[] = "http:{$link}";
                continue;
            }

            if ($link[0] === '/') {
                $result[] = $root . $link;
                continue;
            }

            if (strpos($link, 'http') !== 0) {
                $result[] = $url . $link;
                continue;
            }

            $result[] = $link;
        }

        $newresult = new History();
        $newresult->setType('links')->setUrl($url);
        $newresult->setResult(json_encode([
            'result' => $result
        ]));

        $this->em->beginTransaction();
        try {
            $this->em->persist($newresult);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();

            throw $e;
        }

        return $this->render('default/link_list.twig', ['links' => $result]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Exception
     */
    public function images(Request $request): Response
    {
        $url    = $request->request->get('target');
        $images = $this->finder->findImages($url);

        $parts = [];
        preg_match('#(http(s?):\/\/)?.*\.[A-z\d]+\/?#', $url, $parts);
        $root   = trim($parts[0], '/');
        $result = [];

        foreach ($images as $image) {
            if (empty($image)) {
                continue;
            }

            if (strpos($image, '//') === 0) {
                $result[] = "http:{$image}";
                continue;
            }

            if ($image[0] === '/') {
                $result[] = $root . $image;
                continue;
            }

            if (strpos($image, 'http') !== 0) {
                $result[] = $url . $image;
                continue;
            }

            $result[] = $image;
        }

        $newresult = new History();
        $newresult->setType('images')->setUrl($url);
        $newresult->setResult(json_encode([
            'result' => $result
        ]));

        $this->em->beginTransaction();
        try {
            $this->em->persist($newresult);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();

            throw $e;
        }

        return $this->render('default/image_list.twig', ['images' => $result]);
    }
}
