<?php

namespace Controller;

use lib\EntityManager\EMProvider;
use lib\exceptions\NotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Twig_Loader_Filesystem;
use Twig_Environment;

abstract class Controller
{
    /**
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * Controller constructor.
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function __construct()
    {
        $loader     = new Twig_Loader_Filesystem(__DIR__ . '/../view/');
        $this->twig = new Twig_Environment($loader);
        $this->em   = EMProvider::getEntityManager();
    }

    /**
     * Для корректного возврата 404 из существующих контроллеров
     *
     * @param string $name
     * @param array  $args
     *
     * @throws NotFoundException
     */
    final public function __call(string $name, array $args)
    {
        throw new NotFoundException();
    }

    /**
     * @param string $template
     * @param array  $parameters
     *
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws \InvalidArgumentException
     */
    protected function render(string $template, array $parameters = []): Response
    {
        $text = $this->twig->render($template, $parameters);

        return new Response($text);
    }
}
