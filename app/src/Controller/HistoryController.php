<?php

namespace Controller;

use Entity\History;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HistoryController extends Controller
{
    /**
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws \InvalidArgumentException
     */
    public function index(): Response
    {
        /** @var History[] $results */
        $results     = $this->em->getRepository(History::class)->findAll();
        $resultArray = [];

        foreach ($results as $result) {
            $resultArray[] = $this->getResultMapper()($result);
        }

        return $this->render('default/history.twig', ['history' => $resultArray]);
    }

    private function getResultMapper(): callable
    {
        return function (History $result) {
            return [
                'id'     => $result->getId(),
                'date'   => $result->getDate()->format('Y-m-d h:i:s'),
                'url'    => $result->getUrl(),
                'type'   => $result->getType(),
                'result' => json_decode($result->getResult(), true),
            ];
        };
    }

    /**
     * @param Request $request
     *
     * @return $this|Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws \InvalidArgumentException
     */
    public function show(Request $request)
    {
        $id = $request->query->get('id');
        if (empty($id)) {
            return $this->render('default/result.twig');
        }
        /** @var History $result */
        $result      = $this->em->getRepository(History::class)->findOneBy(['id' => $id]);
        $resultArray = $this->getResultMapper()($result);

        return $this->render('default/result.twig', ['result' => $resultArray]);
    }
}
