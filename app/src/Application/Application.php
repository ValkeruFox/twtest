<?php

namespace Application;

use lib\exceptions\NotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Application
{
    public function run(Request $request): Response
    {
        $controller = $action = 'index';
        $path       = trim($request->getPathInfo(), '/');
        $route      = explode('/', $path, 2);

        empty($route[0]) ?: $controller = $route[0];
        empty($route[1]) ?: $action = $route[1];

        $controller = '\Controller\\' . ucfirst($controller) . 'Controller';

        if (!class_exists($controller)) {
            throw new NotFoundException();
        }

        return (new $controller)->$action($request);
    }
}
