<?php
//Dictrine CLI config file
require_once __DIR__ . '/../src/lib/EntityManager/EMProvider.php';

$dotenv = new \Symfony\Component\Dotenv\Dotenv();
$dotenv->load(__DIR__ . '/../.env');
$em = \lib\EntityManager\EMProvider::getEntityManager();
$em->getConnection()->getDatabasePlatform()
    ->registerDoctrineTypeMapping('enum', 'string');

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($em);

