<?php

use Application\Application;
use Symfony\Component\HttpFoundation\Request;
use lib\exceptions\NotFoundException;
use Symfony\Component\Dotenv\Dotenv;

require_once __DIR__ . '/../vendor/autoload.php';

$request = Request::createFromGlobals();
$app     = new Application();
$dotenv  = new Dotenv();
$dotenv->load(__DIR__ . '/../.env');

try {
    return $app->run($request)->send();
} catch (NotFoundException $e) {
    return $e->render404()->send();
} catch (\Exception $e) {
    echo $e->getMessage();
}
