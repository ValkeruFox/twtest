function changeInputVisibility(value) {
    if (value !== 'text') {
        document.getElementById("testinput").style.display = "none";
    } else {
        document.getElementById("testinput").style.display = "";
    }
}

function handleClick() {

    url = document.getElementById('target').value;
    const regexp = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    if (!regexp.test(url)) {
        alert('Некорректный URL!');
        return;
    }

    selected = document.getElementById('selection').options.selectedIndex;
    category = document.getElementById('selection').options[selected].id;

    try {
        xmlhttp = new XMLHttpRequest();
    } catch (e) {
        alert("Please, update your browser!");
    }

    pattern = '';
    type = document.getElementById('selection').options.selectedIndex;
    if (type === 0) {
        pattern = document.getElementById('indivinput').value;

        if (!pattern) {
            alert('Введите текст для поиска!');
            return;
        }
    }

    category = document.getElementById('selection').options[type].id;

    xmlhttp.open('POST', '/search/' + category, true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send('target=' + url + "&pattern=" + pattern);
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4) {
            if (xmlhttp.status === 200) {
                let div = document.getElementById('result');
                div.innerText = xmlhttp.responseText;
            }
            else {
                alert(xmlhttp.responseText);
            }
        }
    };
}
